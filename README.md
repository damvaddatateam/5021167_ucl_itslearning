# README

This repository contains the code related to the UCL student-success project. More specifically it contains:

1. Code used for extracting and combining data from ESAS and ItsLearning for the data-lake (Data lake). 
2. Code used for downloading data, creating features and training models (Model)
3. Notebooks for analysis. 

## Installation
The code related to the data lake and the model were developed independently and is therefore installed independently. 

### Data lake
All the python-packages used within this project are listed in the `requirements.txt`-file, and can be installed by running:

```bash
pip install -r requirements.txt
```

To interact with the ESAS database you also need to setup a microsoft-sql connection on your computer [Interact with the Microsoft SQL server](#interact-with-the-microsoft-sql-server).

### Model
To install the python packages for the model section use poetry: 
```bash
poetry env use 3.8 # use python 3.8
poetry shell
poetry install
```

## Code structure

### Data lake
The main functionalities are placed in the `src/`-folder. This folder consists of two main-function:

1. `ESAS.py` - The main script for collecting and storing ESAS data on Azure Blob Storage. See [Interacting with Azure blobs](#interacting-with-azure-blobs) for details about working with Azure Blobs and Pandas.

2. `ItsLearning.py` - The main script for collecting and merging ESAS and ItsLearning data, whereafter its stored on Azure Blob Storage. The `ItsLearning.py`-function extracts user information on a daily-level, where several behavioural measures are aggregated into a single feature-vector. See the extracted behavioural statistics in [Extracted columns](#extracted-columns).

Note: The extracted behavioural statistics involve statistics about comparisons within the same course. These comparisons are made by comparison to the course mean the corresponding day, as the time-unit for some of the aggregated session-data is in minutes, and are consequently prone to equal 0. This often induces 0 medians, which makes median comparisons pointless.

The `src/`-folder also includes a `utils/`-folder containing the helper-modules used in the `ESAS.py` and `ItsLeaning.py` scripts. The `utils/config.py`-file contains the configuration of the different external components we use, i.e. ESAS, ItsLearning and Azure. The credentials for these components are set through environment-variables. These include:

1. `ESAS_USER` - A username for the ESAS SQL database.

2. `ESAS_PASSWORD` - A password for the ESAS SQL database corresponding to the username.

3. `ITSLEARNING_USER` - An ItsLearning dataware-house username.

4. `ITSLEARNING_PASSWORD` - An ItsLearning dataware-house password corresponding to the username.

5. `AZURE_STORAGE_CONNECTION_STRING` - An Azure connection-string to the Azure Storage Account used. See the following [link](https://docs.microsoft.com/en-us/azure/storage/common/storage-configure-connection-string) for reference.

An .env file has been made without writing any of these credentials, but to ensure an easy overview of what credentials needs to be set.

### Model
The model functionality is located in the "dropout_prediction" directory. These contain the directories 
1. `dataloader/` : Code to load data from blob storage. Environment variables are imported from the `config.py` and should be set. 
2. `feature_enginering/`: Code to handle feature engineering.  
3. `models/`: Code to train models. There is a seperate `.py`file for each model and a file containing the corresponding class. 
4. Remaining directories are not used. 

## Intended use

### Model
The model is trained on historical data and can only predict dropout on historical data. This is because one of the features is the week number. Once a full years worth of data is available, the model can be retrained and used for predictions. Until then, the model can be evaluated on the test set. 

Begin by downloading the trained models and features from the blob storage frafaldsmodellering/dropout-models/*pkl. 

If you want to retrain the model, you must first generate the features. Please note, if you regenerate the features, you cannot assess the performance of the current model on the new features. First open `main.py` and specify the date ranges you wish to work with. 

To generate features run `python main.py`. This first downloads the daily blobs and combines the data, constructs the features and then dumps features into a single file. 

To analyse results, see notebooks, in particular `5.SHAP analysis.ipynp`. 

To train models run e.g. `python GB.py` to train the Gradient Boosting model. 

### Data lake
The two main-scripts (`ESAS.py` and `ItsLearning.py`) can be run independently, and we suggest that they are run in that way and potentially with different frequencies.

#### ESAS

The ESAS data-collection will serve as the backbone of the data-collection, as ESAS can ensure that all active students are considered - a property we are not guaranteed with the event data from ItsLearning. For the ESAS data-collection, we run a daily CRON-job that runs the `ESAS.py`-script by following command:

```bash
python ESAS.py
```

The script will collect the current days data and post it as a file on the Azure blob storage account "frafaldsmodellering" within the container "daglige-logs". That files will be tagged with meta-data stating that the files have not been enriched with ItsLearning data.

**Important:** As the `ESAS.py`-script depends on the ESAS database, the machine used for running these scripts should be connected to UCLs network.

#### ItsLearning

The ItsLearning data-collection will also run as a CRON-job, but on a weekly basis. In the `ItsLearning.py`-script we need to assemble User and CourseElement lookup tables, which can be quite time-consuming. Consequently, the script is constructed to support enrichment of multiple files, such that a single lookup-creation can be used across various files containing only ESAS-data. The CRON-job should run the following command:

```bash
python ItsLearning.py
```

The script will look in Azure Blob Storage for files with meta-data stating that they have not yet been enriched with ItsLearning-data and begin enriching these. Once enriched the files are posted back to Azure Blob Storage with meta-data stating that they have been enriched.

## Storage details

### Azure ressources

The data-lake is hosted on Azure using the provided UCL subscription. The daily files are stored in a container named `daglige-logs` in Azure Blob Storage under the storage account `frafaldsmodellering`. The security settings for the storage account can be altered to higher and lower level of secure, e.g. only allowing privately connected instances to access the files. We aimed for a slightly over average level security.

### GDPR and anonymization

The collected ESAS data contains multiple person attributable fields such as CPR-number, address, student ID and etc. The only directly person attributable data collected is the student mail, which is hashed using the SHA256-algorithm. All other fields are not directly person attributable, e.g. postal code of home address and education, since there is a theoretical one-to-many relation between such an information and the potential students matching the criteria. Henceforth, these are not anonymized. Furthermore, we believe that knowing their exact phrasing will be beneficial during model construction.

## Extracted columns

**IDs:**

| Variable       |                                                        Description                                                         |
| -------------- | :------------------------------------------------------------------------------------------------------------------------: |
| ExternalUserId | The UCL student ID + the mail-extension. This is the first merge-key used for merging ItsLearning data with the ESAS data. |
| Date           |     The date of the data collected. This is the second merge-key used for merging ItsLearning data with the ESAS data.     |

**Sessions Statistics:**

| Variable                    |                           Description                            |
| --------------------------- | :--------------------------------------------------------------: |
| SessionDurationSeconds_min  |      The shortest general session the user had in seconds.       |
| SessionDurationSeconds_mean | The average length of general sessions the user had in seconds.  |
| SessionDurationSeconds_max  |       The longest general session the user had in seconds.       |
| SessionStartTime_nunique    |           The number of general sessions the user had.           |
| SessionHour_min             |        The earliest hour the user had a general session.         |
| SessionDurationSeconds_max  |         The latest hour the user had a general session.          |
| SessionStartTime_nunique    | The number of unique hours the student had its general sessions. |

**Course Session Statistics:**

| Variable                          |                                          Description                                          |
| --------------------------------- | :-------------------------------------------------------------------------------------------: |
| CourseSessionDurationMinutes_min  |                     The shortest course session the user had in minutes.                      |
| CourseSessionDurationMinutes_mean |                The average length of course sessions the user had in minutes.                 |
| CourseSessionDurationMinutes_max  |                      The longest course session the user had in minutes.                      |
| CourseSessionStartDate_nunique    |                          The number of course sessions the user had.                          |
| CourseSessionHour_min             |                       The earliest hour the user had a course session.                        |
| CourseSessionHour_max             |                        The latest hour the user had a course session.                         |
| CourseSessionHour_nunique         |                The number of unique hours the student had its course sessions.                |
| CourseId_nunique                  |         The number of unique courses where the user interacted with a course session.         |
| AboveMeanSessionDuration_mean     | The fraction of course sessions that were longer than the average session on the same course. |

**Element Session Statistics:**

| Variable                             |                                              Description                                               |
| ------------------------------------ | :----------------------------------------------------------------------------------------------------: |
| ElementSessionDurationMinutes_min    |                         The shortest element session the user had in seconds.                          |
| ElementSessionDurationMinutes_mean   |                    The average length of element sessions the user had in seconds.                     |
| ElementSessionDurationMinutes_max    |                          The longest element session the user had in seconds.                          |
| ElementSessionStartDate_nunique      |                              The number of element sessions the user had.                              |
| IsResource_mean                      |                    The fraction of the element sessions that concerned a resource.                     |
| IsActivity_mean                      |                    The fraction of the element sessions that concerned an activity.                    |
| IsAssessment_mean                    |                   The fraction of the element sessions that concerned an assessment.                   |
| ElementSessionHour_min               |                           The earliest hour the user had a element session.                            |
| ElementSessionHour_max               |                            The latest hour the user had a element session.                             |
| ElementSessionHour_nunique           |                    The number of unique hours the student had its element sessions.                    |
| ElementCourseId_nunique              |                The number of unique courses where the user interacted with an element.                 |
| AboveMeanElementSessionDuration_mean | The fraction of element sessions that were longer than the average element session on the same course. |

## Interacting with Azure blobs

### How to write an Azure blob from Pandas

```python
import pandas as pd
from azure.storage.blob import BlobServiceClient

SERVICE_CLIENT = BlobServiceClient.from_connection_string(AZURE_CONNECTION)
BLOB_CLIENT = SERVICE_CLIENT.get_blob_client(container=CONTAINER_NAME, blob=BLOB_NAME)
blob = data.to_csv(encoding="utf-8", index=False)
BLOB_CLIENT.upload_blob(data=blob)
```

## How to read an Azure blob to Pandas

```python
import pandas as pd
import io
from azure.storage.blob import BlobServiceClient

SERVICE_CLIENT = BlobServiceClient.from_connection_string(AZURE_CONNECTION)
BLOB_CLIENT = SERVICE_CLIENT.get_blob_client(container=CONTAINER_NAME, blob=BLOB_NAME)
data = pd.read_csv(io.StringIO(BLOB_CLIENT.download_blob().content_as_text()))
```

## Interact with the Microsoft SQL server

### MAC OS

To interact with the mssql server you need to have the correct drivers installed.
On mac these can be installed by following
[the documentation from Microsoft](https://docs.microsoft.com/en-us/sql/connect/odbc/linux-mac/install-microsoft-odbc-driver-sql-server-macos?view=sql-server-ver15):

```bash
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
brew tap microsoft/mssql-release https://github.com/Microsoft/homebrew-mssql-release
brew update
HOMEBREW_NO_ENV_FILTERING=1 ACCEPT_EULA=Y brew install msodbcsql17 mssql-tools
```

### LINUX (Debian)

If you want to install instead on debian or in a docker container using a buster image, you could should install the drivers with the following commands:

```bash
sudo su
curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -

#Download appropriate package for the OS version
#Choose only ONE of the following, corresponding to your OS version

#Debian 8
curl https://packages.microsoft.com/config/debian/8/prod.list > /etc/apt/sources.list.d/mssql-release.list

#Debian 9
curl https://packages.microsoft.com/config/debian/9/prod.list > /etc/apt/sources.list.d/mssql-release.list

#Debian 10
curl https://packages.microsoft.com/config/debian/10/prod.list > /etc/apt/sources.list.d/mssql-release.list

exit
sudo apt-get update
sudo ACCEPT_EULA=Y apt-get install -y msodbcsql17
# optional: for bcp and sqlcmd
sudo ACCEPT_EULA=Y apt-get install -y mssql-tools
echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc
source ~/.bashrc
# optional: for unixODBC development headers
sudo apt-get install -y unixodbc-dev
# optional: kerberos library for debian-slim distributions
sudo apt-get install -y libgssapi-krb5-2
```
