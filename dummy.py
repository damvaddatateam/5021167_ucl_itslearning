from dropout_prediction.models.model_training import ModelFactory
import sklearn

my_dict = {
    "name": "testingDT",
    "model": sklearn.tree.DecisionTreeClassifier,
    "model_param": {
        "int": {
            "max_depth": {"low": 2, "high": 64},
            "min_samples_split": {"low": 2, "high": 4},
        },
        "categorical": {"class_weight": ["balanced"]},
    },
}

n_trials = 10

mf = ModelFactory(my_dict)
mf.load_data("dropout_prediction/data/features.csv")
mf.optimize("maximize", n_trials)
