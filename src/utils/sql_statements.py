# Get the students that started after jan. 2021 that are currently active, and not expected to end until 60 days ago
# ALL the tabels will be collected using the esas_personid

ESAS_ACTIVE_STUDENTS = """ SELECT dbo.Person.esas_personid, dbo.Person.esas_cpr_nummer, dbo.Personoplysning.esas_lokalt_studienummer,  dbo.Studieforloeb.esas_studiestart,dbo.Studieforloeb.esas_forventet_afslutning, dbo.Studieforloeb.esas_afgangsdato, 
                  dbo.Uddannelsesaktivitet.esas_navn AS Uddannelse, dbo.Uddannelsesaktivitet.esas_dst_kode, dbo.Personoplysning.esas_rolle, dbo.Land.esas_iso2, dbo.Personoplysning.esas_studieemail, dbo.Person.FirstName, dbo.Person.LastName, 
                  dbo.Person.Address1_Line1, dbo.Postnummer.esas_by, dbo.Postnummer.esas_postnummer, dbo.Studieforloeb.esas_studieforloebId, dbo.Uddannelsesaktivitet.esas_uddannelsestype, dbo.Person.MiddleName, dbo.Person.FullName,
                  dbo.Person.esas_cpr_personstatus, osv.Label as Uddannelses_label, ov.Label as Person_label, na.esas_navn as Afgangsaarsag, na.esas_central_afgangsaarsag
FROM     dbo.Land INNER JOIN
                  dbo.Person 
                  INNER JOIN
                  dbo.Personoplysning ON dbo.Person.ContactId = dbo.Personoplysning.esas_person_id ON dbo.Land.esas_landId = dbo.Person.esas_land_id 
                  INNER JOIN
                  dbo.Postnummer ON dbo.Person.esas_postnummer_by_id = dbo.Postnummer.esas_postnummerId AND dbo.Land.esas_landId = dbo.Postnummer.esas_land_id 
                  INNER JOIN
                  dbo.Uddannelsesstruktur 
                  INNER JOIN
                  dbo.Studieforloeb ON dbo.Uddannelsesstruktur.esas_uddannelsesstrukturId = dbo.Studieforloeb.esas_uddannelsesstruktur_id
                  INNER JOIN
                  dbo.Uddannelsesaktivitet ON dbo.Uddannelsesstruktur.esas_uddannelsesaktivitet_id = dbo.Uddannelsesaktivitet.esas_uddannelsesaktivitetId 
                  INNER JOIN
                  dbo.OptionSetValueString as osv ON dbo.Uddannelsesstruktur.esas_uddannelsestype = osv.Value
                  	AND osv.AttributeName = 'esas_uddannelsestype' 
	                AND osv.EntityType = 'Uddannelsesaktivitet' ON dbo.Person.ContactId = dbo.Studieforloeb.esas_studerende_id   
	             INNER JOIN
                  dbo.OptionSetValueString as ov ON dbo.Person.esas_cpr_personstatus = ov.Value
                  	AND ov.AttributeName = 'esas_cpr_personstatus' 
	                AND ov.EntityType = 'Person' 
	             FULL OUTER JOIN 
	             dbo.NationalAfgangsaarsag na 
	             ON na.esas_national_afgangsaarsagId  = dbo.Studieforloeb.esas_national_afgangsaarsag_id 
WHERE  (dbo.Studieforloeb.esas_studiestart > '2021-01-01' AND osv.Label='Ordinær uddannelse' AND (dbo.Studieforloeb.esas_afgangsdato > DATEADD(day, - 30, GETDATE()) OR dbo.Studieforloeb.esas_afgangsdato is null ))"""

ESAS_INTERN_NOT_PASSED = """ SELECT A.esas_studieforloebId, R.* 
FROM v_damvad_aktive A CROSS APPLY
(
SELECT CASE WHEN COUNT(*) > 0 THEN 1 ELSE 0 END AS Praktik_Aflyst
FROM
Praktikophold P INNER JOIN
                  dbo.Bedoemmelse B ON P.esas_gennemfoerelsesuddannelseselement_id = B.esas_gennemfoerelsesuddannelseselement_id
WHERE A.esas_studieforloebId = P.esas_studieforloeb_id AND (B.esas_bestaaet = 0) AND P.esas_slutdato < GETDATE()
) R
"""

ESAS_LATEST_RESULT = """ SELECT  A.esas_studieforloebId, R.esas_bedoemmelsesdato, R.esas_bestaaet, R.esas_taeller_som_forsoeg, dbo.Karakter.esas_karakter 
FROM v_damvad_aktive A CROSS APPLY
(
SELECT TOP 1 esas_bedoemmelsesdato, esas_bestaaet, esas_taeller_som_forsoeg, esas_karakter_id FROM Bedoemmelse B
WHERE 
A.esas_studieforloebId = B.esas_studieforloeb_id 
ORDER BY esas_bedoemmelsesdato DESC
) R
FULL OUTER JOIN dbo.Karakter ON dbo.Karakter.esas_karakterId  = R.esas_karakter_id """

ESAS_NUMBER_OF_INACTIVE_PERIODES = """ SELECT A.esas_studieforloebId, R.*
FROM v_damvad_aktive A CROSS APPLY
(
SELECT CASE WHEN COUNT(*) > 0 THEN COUNT(*) ELSE 0 END AS inaktivperiode
FROM
StudieinaktivPeriode S
WHERE A.esas_studieforloebId = S.esas_studieforloeb_id  
) R"""
