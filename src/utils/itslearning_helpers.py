from datetime import datetime
import httpx

from .config import DATA_WAREHOUSE_URL, DATA_WAREHOUSE_CREDENTIALS

STATUS = {'next_url': ''}


def get_session_data(
    table_name: str,
    date: str = datetime.now().strftime("%Y-%m-%d"),
):
    """Collects daily session data from the provided table at the provided date.

    Args:
        table_name (str): The ItsLearning table name.
        date (str, optional): The date to collect from in a (%Y-%m-%d)-format. Defaults to datetime.now().strftime("%Y-%m-%d").

    Returns:
        pd.DataFrame: A pandas dataframe with all the available ItsLearning data.
    """
    STATUS[table_name] = 0
    date = datetime.strptime(date, "%Y-%m-%d")

    print(f"Collecting {table_name} from {date}...")

    url = f'{DATA_WAREHOUSE_URL}/{table_name}?'
    url += f'$filter=Date/DateId eq {date.strftime("%Y%m%d")}'
    results = []
    # Loop pages in table in data warehouse
    while url is not None:

        response = httpx.get(url, auth=DATA_WAREHOUSE_CREDENTIALS, timeout=60)

        # Check that request was successful and get JSON response
        response.raise_for_status()
        data = response.json()

        # Save data to file
        results.extend(data['value'])

        # Get link to next page in table
        url = data.get('odata.nextLink', None)
        STATUS['next_url'] = url

        STATUS[table_name] += len(data['value'])
        print(STATUS)

    return results


def get_lookup_table_data(
    table_name: str,
    query_params: str = "",
):
    """Collects the all the rows of a lookup table that matches the provided query_params.

    Args:
        table_name (str): The ItsLearning table name.
        query_params (str, optional): The Odata query that should be applied when querying the lookup table. Defaults to "".

    Returns:
        pd.DataFrame: A pandas dataframe with all the available ItsLearning data.
    """

    STATUS[table_name] = 0

    url = f'{DATA_WAREHOUSE_URL}/{table_name}'
    url = url + query_params if query_params else url

    results = []
    while url is not None:
        response = httpx.get(url, auth=DATA_WAREHOUSE_CREDENTIALS, timeout=60)

        # Check that request was successful and get JSON response
        response.raise_for_status()
        data = response.json()

        # Save data to file
        results.extend(data['value'])

        # Get link to next page in table
        url = data.get('odata.nextLink', None)
        STATUS['next_url'] = url

        STATUS[table_name] += len(data['value'])
        print(STATUS)

    return results
