import os
import pyodbc
from azure.storage.blob import BlobServiceClient

# ESAS configuration:
USERNAME = os.getenv("ESAS_USER")
PASSWORD = os.getenv("ESAS_PASSWORD")
server = "sql17srv01"
database = "ESAS"
CNXN = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+USERNAME+';PWD='+ PASSWORD)


# ItsLearning configuration:
DATA_WAREHOUSE_URL = 'https://eu1reporting.itslearning.com/odata/ucl/'
DATA_WAREHOUSE_CREDENTIALS = (
    os.environ['ITSLEARNING_USER'],
    os.environ['ITSLEARNING_PASSWORD'],
)


# Azure configuration:
AZURE_CONNECTION = os.getenv('AZURE_STORAGE_CONNECTION_STRING')
SERVICE_CLIENT = BlobServiceClient.from_connection_string(AZURE_CONNECTION)
CONTAINER_NAME = "daglige-logs"
