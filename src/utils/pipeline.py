import pandas as pd

from .itslearning_helpers import get_lookup_table_data


class SQLContextManager:
    """Generic Class for querying SQL data and converting it into pandas Dataframes."""

    def __init__(self, conn) -> None:
        self.conn = conn

    def query(self, query_string: str) -> pd.DataFrame:
        df = pd.read_sql(query_string, self.conn)
        return df


class Table(object):
    """Generic class for managing lookups from ItsLearning"""

    def __init__(self, name, query=""):
        self.name = name
        self.query = query
        self.data = None

    def read_from_file(self):
        self.data = pd.read_json(f"Data/{self.name}.json", orient="records")

    def save_to_file(self):
        self.data.to_json(f"Data/{self.name}.json", orient="records")

    def collect(self):
        data = get_lookup_table_data(
            table_name=self.name,
            query_params=self.query,
        )
        self.data = pd.DataFrame(data)


def engineer_user_sessions(data):
    """Helper for creating additional features on the raw UserSessions data.

    Args:
        data (pd.DataFrame): The UserSessions data.

    Returns:
        pd.DataFrame: The UserSessions data with additional features used later in the pipeline.
    """
    # Add in which hour of the day the session elapsed:
    data["SessionHour"] = data.SessionStartTime.apply(lambda x: x.hour)
    return data


def engineer_course_sessions(data):
    """Helper for creating additional features on the raw UserCourseSessions data.

    Args:
        data (pd.DataFrame): The UserCourseSessions data.

    Returns:
        pd.DataFrame: The UserCourseSessions data with additional features used later in the pipeline.
    """
    # Add in which hour of the day the session elapsed:
    data["CourseSessionHour"] = data.CourseSessionStartDate.apply(lambda x: x.hour)

    # Add the average session duration this day for sessions in the same course:
    data["CourseSessionDurationMinutes"] = data["CourseSessionDurationMinutes"].astype(
        float
    )
    course_lookup = (
        data.groupby("CourseId")
        .agg({"CourseSessionDurationMinutes": ["mean"]})
        .reset_index()
    )
    course_lookup.columns = [
        "_".join(x) if x[1] else x[0] for x in course_lookup.columns.ravel()
    ]
    data = data.merge(course_lookup, how="left")

    # Create an indicator of whether the session was longer than the average session on the same course:
    data["AboveMeanSessionDuration"] = (
        data["CourseSessionDurationMinutes"]
        >= data["CourseSessionDurationMinutes_mean"]
    )

    # Drop the course mean session length.
    data.drop("CourseSessionDurationMinutes_mean", axis=1, inplace=True)
    return data


def engineer_course_element_sessions(data, lookup):
    """Helper for creating additional features on the raw UserElementSessions data.

    Args:
        data (pd.DataFrame): The UserElementSessions data.
        lookup (pd.DataFrame): The CourseElements lookup, used for adding element meta-data.

    Returns:
        pd.DataFrame: The UserElementSessions data with additional features used later in the pipeline.
    """
    # Add in which hour of the day the session elapsed:
    data["ElementSessionHour"] = data.ElementSessionStartDate.apply(lambda x: x.hour)

    # Merge metadata about the CourseElement:
    data = data.merge(
        lookup.data[
            ["CourseId", "CourseElementId", "IsResource", "IsActivity", "IsAssessment"]
        ]
    )

    # Add the average session duration this day for sessions in the same course:
    data = data.rename(columns={"CourseId": "ElementCourseId"})
    data["ElementSessionDurationMinutes"] = data[
        "ElementSessionDurationMinutes"
    ].astype(float)
    course_lookup = (
        data.groupby("ElementCourseId")
        .agg({"ElementSessionDurationMinutes": ["mean"]})
        .reset_index()
    )
    course_lookup.columns = [
        "_".join(x) if x[1] else x[0] for x in course_lookup.columns.ravel()
    ]
    data = data.merge(course_lookup, how="left")

    # Create an indicator of whether the session was longer than the average session on the same course:
    data["AboveMeanElementSessionDuration"] = (
        data["ElementSessionDurationMinutes"]
        >= data["ElementSessionDurationMinutes_mean"]
    )

    # Drop the course mean session length.
    data.drop("ElementSessionDurationMinutes_mean", axis=1, inplace=True)

    return data
