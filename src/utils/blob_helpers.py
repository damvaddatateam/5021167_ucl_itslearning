from typing import List, Dict, Any
import pandas as pd
import io

from .config import SERVICE_CLIENT, CONTAINER_NAME


def get_new_blobs() -> List[str]:
    """Queries Azure blob storage for all the blobs that have not been enriched with ItsLearning data.

    Returns:
        List[str]: A list of all the blob names of the files that have not been enriched.
    """
    CONTAINER_CLIENT = SERVICE_CLIENT.get_container_client(container=CONTAINER_NAME)
    new_blobs = []
    for blob in CONTAINER_CLIENT.list_blobs(include="metadata"):
        tags = blob.get("metadata") or {}
        if tags.get("Enriched") != "True":
            new_blobs.append(blob["name"])
    return new_blobs


def read_blob(blob_name: str) -> pd.DataFrame:
    """Read a csv-blob from the Azure Blob Container specified in config.py into a pandas dataframe.

    Args:
        blob_name (str): The name of the blob without the .csv suffix

    Returns:
        pd.DataFrame: A dataframe of the csv-files content.
    """

    BLOB_CLIENT = SERVICE_CLIENT.get_blob_client(
        container=CONTAINER_NAME, blob=blob_name
    )
    data = pd.read_csv(io.StringIO(BLOB_CLIENT.download_blob().content_as_text()))
    return data


def write_blob(blob_name: str, data: pd.DataFrame, metadata: Dict[Any, Any] = {}) -> None:
    """Write a pandas DataFrame to a Azure csv-blob under the container specified in config.py.

    Args:
        blob_name (str): The name of the written blob.
        data (pd.DataFrame): The data to wrtie to Azure Blob Storage.
        metadata (Dict[Any, Any], optional): The metadata to append to the blob. Defaults to {}.
    """
    BLOB_CLIENT = SERVICE_CLIENT.get_blob_client(
        container=CONTAINER_NAME, blob=blob_name
    )
    blob = data.to_csv(encoding="utf-8", index=False)
    BLOB_CLIENT.upload_blob(
        data=blob,
        metadata=metadata,
        overwrite=True,
    )
