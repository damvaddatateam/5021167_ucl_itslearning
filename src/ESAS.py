from datetime import datetime
from hashlib import sha256
from loguru import logger

import utils.sql_statements as ss
from utils.pipeline import SQLContextManager
from utils.config import CNXN
from utils.blob_helpers import write_blob


DROP_COLS = [
    "esas_personid",
    "esas_cpr_nummer",
    "esas_lokalt_studienummer",
    "FirstName",
    "LastName",
    "Address1_Line1",
    "MiddleName",
    "FullName",
]


def extract_daily_esas_date(date: object = datetime.now()) -> None:
    """Extracts the current ESAS data and appends temporal information corresponding to the specified date.

    Args:
        date (object, optional): A datetime-object specifying a particular time-point. Defaults to datetime.now(), e.g. the current time.
    """
    logger.info(f"Collecting ESAS data for {date.strftime('%Y-%m-%d')}...")

    # Instantiate SQL manager:
    logger.info("\tQuerying ESAS...")
    sql_manager = SQLContextManager(conn=CNXN)

    # Collect all relevant data from esas and add the date and the weekday
    base_view = sql_manager.query(ss.ESAS_ACTIVE_STUDENTS)
    praktik = sql_manager.query(ss.ESAS_INTERN_NOT_PASSED)
    latest_result = sql_manager.query(ss.ESAS_LATEST_RESULT)
    num_inactive_periods = sql_manager.query(ss.ESAS_NUMBER_OF_INACTIVE_PERIODES)

    for sub_query in [praktik, latest_result, num_inactive_periods]:
        base_view = base_view.merge(
            sub_query,
            left_on="esas_studieforloebId",
            right_on="esas_studieforloebId",
            how="left",
        )

    # Keep only students with 4 character and 5 digit student indentifier:
    base_view = base_view[
        base_view.esas_studieemail.notna()
        & base_view.esas_studieemail.str.match(r"[a-z]{4}\d{5}\@edu\.ucl\.dk")
    ]

    logger.info(f"\tFound total of {len(base_view)} students...")
    base_view["Date"] = date.strftime("%Y-%m-%d")
    base_view["Weekday"] = date.weekday()
    base_view.drop(DROP_COLS, axis=1, inplace=True)

    base_view["esas_studieemail"] = base_view["esas_studieemail"].apply(
        lambda x: sha256(x.strip().encode()).hexdigest()
    )

    # Save the data on Azure in blob storage
    logger.info("\tDispatching data to Azure BLOB storage...")
    write_blob(
        blob_name=f"data_{date.strftime('%Y-%m-%d')}.csv",
        data=base_view,
        metadata={"Enriched": "False"},
    )


if __name__ == "__main__":
    extract_daily_esas_date()
