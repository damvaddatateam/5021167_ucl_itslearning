import pandas as pd
from hashlib import sha256
from loguru import logger

from utils import itslearning_helpers
from utils import pipeline
from utils.blob_helpers import get_new_blobs, read_blob, write_blob


# Pipeline configuration:
TABLE_CONFIG = {
    "UserSessions": {
        "initial_columns": ["UserId", "SessionStartTime", "SessionDurationSeconds"],
        "date_columns": [("SessionStartTime", "%Y-%m-%dT%H:%M:%S")],
        "engineering_callable": pipeline.engineer_user_sessions,
        "aggregation_key": "UserId",
        "aggregation_config": {
            "SessionDurationSeconds": ["min", "mean", "max"],
            "SessionHour": ["nunique", "min", "max"],
            "SessionStartTime": ["nunique"],
        },
        "merge_rule": None,
    },
    "UserCourseSessions": {
        "initial_columns": [
            "UserId",
            "CourseId",
            "CourseSessionStartDate",
            "CourseSessionDurationMinutes",
        ],
        "date_columns": [("CourseSessionStartDate", "%Y-%m-%dT%H:%M:%S")],
        "engineering_callable": pipeline.engineer_course_sessions,
        "aggregation_key": "UserId",
        "aggregation_config": {
            "CourseSessionDurationMinutes": ["min", "mean", "max"],
            "CourseSessionHour": ["nunique", "min", "max"],
            "CourseSessionStartDate": ["nunique"],
            "CourseId": ["nunique"],
            "AboveMeanSessionDuration": ["mean"],
        },
        "merge_rule": "outer",
    },
    "UserElementSessions": {
        "initial_columns": [
            "UserId",
            "CourseId",
            "CourseElementId",
            "ElementSessionStartDate",
            "ElementSessionDurationMinutes",
        ],
        "date_columns": [("ElementSessionStartDate", "%Y-%m-%dT%H:%M:%S")],
        "engineering_callable": pipeline.engineer_course_element_sessions,
        "aggregation_key": "UserId",
        "aggregation_config": {
            "ElementSessionDurationMinutes": ["min", "mean", "max"],
            "ElementSessionHour": ["nunique", "min", "max"],
            "ElementSessionStartDate": ["nunique"],
            "ElementCourseId": ["nunique"],
            "IsResource": ["mean"],
            "IsActivity": ["mean"],
            "IsAssessment": ["mean"],
            "AboveMeanElementSessionDuration": ["mean"],
        },
        "merge_rule": "outer",
    },
}


def main(
    dates: list,
    write_lookups: bool = False,
    read_from_file: bool = False,
):
    """The main function used for querying the latest ItsLearning data.
    The function first fetches the necessary lookup tables from ItsLearning used for creating features,
    then it goes through the provided list of dates and fetches the user interaction-data.
    Finally it retrieves the ESAS data from the same day, updates it with ItsLearning data and restores it on Azure blob storage.

    Args:
        dates (list): A list of (%Y-%m-%d)-formatted dates that points to the log-files that should be enriched with ItsLearning data.
        write_lookups (bool, optional): Whether to write the lookups retrieved initially to the local Data/-folder, for quick access in later runs. Defaults to False.
        read_from_file (bool, optional): Whether to read the lookups retrieved initially from the local Data/-fodler instead of collecting them from ItsLearning. Defaults to False.
    """

    logger.info(f"Collecting IsLearning data for {len(dates)} new files...")

    # Initialize the lookup table dataclasses:
    users = pipeline.Table(name="Users")
    course_elements = pipeline.Table(
        name="CourseElements", query="?$filter=ElementStatus eq 'Active'"
    )

    # Collect the lookup table data either from local files if read_from_file=True, otherwise by collecting it from ItsLearning.
    if read_from_file:
        logger.info("\tAssembling lookup tables from local files...")
        users.read_from_file()
        course_elements.read_from_file()
    else:
        logger.info("\tAssembling lookup tables from ItsLearning...")
        users.collect()
        course_elements.collect()

    # If write_lookups=True, we write the retrieved lookup data to files in the Data/-folder.
    if write_lookups:
        logger.info("\tWriting local lookup tables...")
        users.save_to_file()
        course_elements.save_to_file()

    logger.info("\tEnriching new files...")
    for date in dates:
        logger.info(f"\t\tEnriching file from {date}...")

        # Run through the pipeline configuration :
        for key, value in TABLE_CONFIG.items():

            # Query data from the corresponding key/table:
            data = itslearning_helpers.get_session_data(
                key,
                date=date,
            )
            data = pd.DataFrame(data)

            # Convert date columns to datetime datatype:
            data = data[value["initial_columns"]]
            for column, date_format in value["date_columns"]:
                data[column] = pd.to_datetime(data[column], format=date_format)

            # Run the feature engineering function on the data:
            data = (
                value["engineering_callable"](data, course_elements)
                if key == "UserElementSessions"
                else value["engineering_callable"](data)
            )

            # Aggregate the data to one row per user:
            for column in value["aggregation_config"].keys():
                if column not in [
                    name for name, _ in value["date_columns"]
                ] and not column.endswith("Id"):
                    data[column] = data[column].astype(float)

            data = (
                data.groupby(value["aggregation_key"])
                .agg(value["aggregation_config"])
                .reset_index()
            )
            data.columns = ["_".join(x) if x[1] else x[0] for x in data.columns.ravel()]

            # Update the aggregate Itslearning table with the data from the current table:
            if value["merge_rule"] is None:
                table = data.copy()
            else:
                table = table.merge(data, how=value["merge_rule"])

        # Merge in user data:
        table = users.data[["UserId", "ExternalUserId"]].merge(table, how="right")

        # Filter to students with 4 lowercase letters and 5 digits as UCL username:
        table = table[table.ExternalUserId.str.match(r"[a-z]{4}\d{5}")]

        # Update columns to match ESAS keys:
        table["ExternalUserId"] = table["ExternalUserId"] + "@edu.ucl.dk"
        table["ExternalUserId"] = table["ExternalUserId"].apply(lambda x: sha256(x.strip().encode()).hexdigest())
        table["Date"] = date

        # Collect the ESAS data from Azure blob storage:
        blob_name = f"data_{date}.csv"
        data = read_blob(blob_name=blob_name)

        # Merge in ItsLearning data:
        data = data.merge(table, how="left", left_on=["esas_studieemail", "Date"], right_on=["ExternalUserId", "Date"])
        data.drop(["UserId", "ExternalUserId"], axis=1, inplace=True)

        # Save back to Azure blob storage:
        write_blob(blob_name=blob_name, data=data, metadata={"Enriched": "True"})


if __name__ == "__main__":
    # Collect blobs without ItsLearning data:
    blob_names = get_new_blobs()

    # Get the dates from the blobs:
    dates = [name.replace("data_", "").replace(".csv", "") for name in blob_names]

    # Update all the blobs with ItsLearning data:
    main(dates=dates)
