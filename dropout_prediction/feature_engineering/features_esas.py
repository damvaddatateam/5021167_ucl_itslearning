import pandas as pd

from dropout_prediction.feature_engineering.utils.utils import (
    distance_to_campus_mapping,
    grade_mapping,
    grade_passed_mapping,
)


def will_drop_out_in_n_days(window_data: pd.DataFrame, n_days: int) -> int:
    """Boolean [0, 1] value if student will drop out within the next n days"""

    will_drop_out = (
        0
        if window_data["dropout_label"][-1] == 0
        else int(
            (window_data["dropout_date"][-1] - window_data.index[-1]).days < n_days
        )
    )
    return will_drop_out


def get_number_of_address_changes(window_data: pd.DataFrame) -> int:
    """Get the number of address changes within the scope of the
    windowed data"""

    n_address_changes = window_data["esas_postnummer"].nunique() - 1
    return n_address_changes


def get_cancelled_internship(window_data: pd.DataFrame) -> int:
    """Boolena [0, 1] value if the student have had a cancelled internship
    within the scope of the windowed data"""

    intership_cancelled = int(window_data["Praktik_Aflyst"].any())
    return intership_cancelled


def get_latest_grade(window_data: pd.DataFrame) -> int:
    """Get the latest grade for the student within the
    scope of the windowed data"""

    last_grade = window_data["esas_karakter"][-1]
    return grade_mapping[last_grade]


def get_latest_grade_passing_score(window_data: pd.DataFrame) -> int:
    """Get if the latest grade was a 'passed' or a 'failed' for the
    student within the scope of the windowed data"""

    last_grade = window_data["esas_karakter"][-1]
    return grade_passed_mapping[last_grade]


def get_distance_to_ucl_campus(window_data: pd.DataFrame) -> float:
    """Get the distance from the students home address to the UCL campus"""

    edu = window_data["Uddannelse"][-1]
    postal_code = window_data["esas_postnummer"][-1]
    # handle the plethora of zip codes in Copenhagen
    if postal_code > 1000 and postal_code < 1500:
        postal_code = "1000-1499"
    elif postal_code > 1500 and postal_code < 1799:
        postal_code = "1500-1799"
    elif postal_code > 1800 and postal_code < 1900:
        postal_code = "1800-1899"

    return distance_to_campus_mapping[edu].get(str(postal_code), 1000)


def get_days_since_last_grade(window_data: pd.DataFrame) -> int:
    """Get the number of days since last grade was recieved"""

    delta = window_data.index.max() - window_data["esas_bedoemmelsesdato"].max()
    return delta.days


def get_n_grades(window_data: pd.DataFrame) -> int:
    """Get the number of grades the student has received within
    the scope of the windowed data"""

    unique_grades = window_data["esas_bedoemmelsesdato"].unique()
    grades_witin_scope = (unique_grades < window_data.index.max()) & (
        unique_grades > window_data.index.min()
    )
    return grades_witin_scope.sum()


def get_days_since_edu_start(window_data: pd.DataFrame) -> int:
    """Get the number of days since the student started the education"""
    delta = window_data.index.max() - window_data["esas_studiestart"].max()
    return delta.days


def get_days_to_edu_end(window_data: pd.DataFrame) -> int:
    """Get the number of days before the student is expected to end his/her education"""
    delta = window_data["esas_forventet_afslutning"].max() - window_data.index.max()
    return delta.days
