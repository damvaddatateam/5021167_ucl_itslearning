import numpy as np
import pandas as pd


def get_number_of_sessions(window_data: pd.DataFrame) -> int:
    """Get number of sessions within the scope of the window"""
    return window_data["SessionStartTime_nunique"].fillna(0).sum().astype(int)


def get_number_of_days_with_sessions(window_data: pd.DataFrame) -> int:
    """Get the number of distinct days with active sessions within the
    scope of the window"""
    return window_data["SessionHour_min"].notna().sum()


def get_average_daily_session_duration(window_data: pd.DataFrame) -> float:
    """Get the average daily session data in the scope of the window"""
    return window_data["SessionDurationSeconds_mean"].mean()


def get_min_value_in_window(window_data: pd.DataFrame, variable: str) -> float:
    """Get the min value of a single variable in the window data"""
    return window_data[variable].min()


def get_max_value_in_window(window_data: pd.DataFrame, variable: str) -> float:
    """Get the max value of a single variable in the window data"""
    return window_data[variable].max()


def get_median_value_in_window(window_data: pd.DataFrame, variable: str) -> float:
    """Get the median value of a single variable in the window data"""
    if window_data[variable].isna().all():
        median = np.nan
    else:
        median = window_data[variable].median()
    return median


def get_mode_value_in_window(window_data: pd.DataFrame, variable: str) -> float:
    """Get the mode value of a single variable in the window data"""

    if window_data[variable].isna().all():
        mode = np.nan
    else:
        mode = window_data[variable].mode()[0]
    return mode
