import datetime

import pandas as pd
from tqdm import tqdm

import dropout_prediction.feature_engineering.features_esas as esas
import dropout_prediction.feature_engineering.features_its_learning as il


class WindowFeatureEngine:
    """Calculate features realted to fixed date windows"""

    def __init__(self, data: pd.DataFrame, date_grid: pd.DataFrame) -> None:
        self.data = data
        self.date_grid = date_grid
        self.lookback_window_lengths = [30]

    def _get_dataframe_window(self, start_date, end_date):
        """Filter dataframe on date range given by start date and end date"""
        return self.data.loc[start_date:end_date]

    def get_lookback_dataframe(self, date, lookback_window_length: int):
        """Filter dataframe on date range given by start date and lookback"""
        start_date = date + datetime.timedelta(days=-lookback_window_length)
        end_date = date
        dataframe_window = self._get_dataframe_window(start_date, end_date)

        return dataframe_window

    def compute_features_from_config(self):
        pass

    def compute_all_features_for_all_windows(self) -> pd.DataFrame:
        """Compute all features for all window lengths"""

        # TODO: implement this
        # Calculate all features for various window lenghts
        for window_length in self.lookback_window_lengths:
            print(f"Computing features for last {window_length} day(s) window...")

    def compute_features_from_date_grid(
        self, lookback_window_length: int
    ) -> pd.DataFrame:
        """Compute all features from date grid"""

        # TODO: this can give more days than expected from the date range
        unique_dates = pd.to_datetime(self.date_grid.reset_index()["date"].unique())

        list_of_df = []
        for date in tqdm(unique_dates, desc="Dates"):

            # Get the windowed dataframe needed to calculate features for all students
            window_data = self.get_lookback_dataframe(date, lookback_window_length)

            # Group by student and apply feature extraction fuction
            window_features = window_data.groupby("esas_studieforloebId").apply(
                lambda group: self.compute_all_features_for_window(date, group)
            )
            list_of_df.append(window_features)

        # Combine features for each day to a single dataframe
        feature_df = (
            pd.concat(list_of_df, axis=0)
            .reset_index()
            .set_index(["date", "esas_studieforloebId"])
        )

        return feature_df

    def compute_all_features_for_window(self, date, window_data: pd.DataFrame):
        """Compute all features for each student witin a given date range"""

        # Compute features on window
        window_features = {
            "date": date,
            "dropout_label": esas.will_drop_out_in_n_days(window_data, 90),
            "dropout_date": window_data["dropout_date"][-1],
            # ESAS Features
            "education": window_data["Uddannelse"][-1],
            "address_changes": esas.get_number_of_address_changes(window_data),
            "internship_cancelled": esas.get_cancelled_internship(window_data),
            "latest_grade": esas.get_latest_grade(window_data),
            "latest_grade_pass": esas.get_latest_grade_passing_score(window_data),
            "days_since_last_evaluation": esas.get_days_since_last_grade(window_data),
            "number_of_evaluations": esas.get_n_grades(window_data),
            "days_since_edu_start": esas.get_days_since_edu_start(window_data),
            "days_to_edu_end": esas.get_days_to_edu_end(window_data),
            "distance_to_ucl_campus": esas.get_distance_to_ucl_campus(window_data),
            # It's Learning Features
            "number_of_sessions": il.get_number_of_sessions(window_data),
            "number_of_active_days": il.get_number_of_days_with_sessions(window_data),
            "avg_daily_session_duration": il.get_average_daily_session_duration(
                window_data
            ),
            "mode_earliest_hour_session": il.get_mode_value_in_window(
                window_data, "SessionHour_min"
            ),
            "median_earliest_hour_session": il.get_median_value_in_window(
                window_data, "SessionHour_min"
            ),
            "mode_latest_hour_session": il.get_mode_value_in_window(
                window_data, "SessionHour_max"
            ),
            "median_latest_hour_session": il.get_median_value_in_window(
                window_data, "SessionHour_max"
            ),
            "mode_number_of_course_sessions_per_day": il.get_mode_value_in_window(
                window_data, "CourseSessionHour_nunique"
            ),
            "median_number_of_course_sessions_per_day": il.get_mode_value_in_window(
                window_data, "CourseSessionHour_nunique"
            ),
            "median_above_session_duration": il.get_mode_value_in_window(
                window_data, "AboveMeanSessionDuration_mean"
            ),
            "min_above_session_duration": il.get_mode_value_in_window(
                window_data, "AboveMeanSessionDuration_mean"
            ),
            "max_above_session_duration": il.get_mode_value_in_window(
                window_data, "AboveMeanSessionDuration_mean"
            ),
        }

        window_series = pd.Series(window_features)

        return window_series
