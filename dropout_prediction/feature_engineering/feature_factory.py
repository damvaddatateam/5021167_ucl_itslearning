import pandas as pd

from dropout_prediction.feature_engineering.window_features import WindowFeatureEngine


class FeatureFactory:
    """Class to compute all the features"""

    def __init__(self, preprocessed_data: pd.DataFrame) -> None:
        self.preprocessed_data = self._prepare_data(preprocessed_data)

    @staticmethod
    def _prepare_data(data: pd.DataFrame) -> pd.DataFrame:
        """Change the index to a multiindex with date
        and studentforloebId"""
        data.sort_values(by="Date", inplace=True)
        data.set_index("Date", inplace=True)
        return data

    @staticmethod
    def _postprocess_data(data: pd.DataFrame) -> pd.DataFrame:
        """Fix missing values in features"""

        # ESAS Features
        data["latest_grade"] = data["latest_grade"].fillna(-100)
        data["days_since_last_evaluation"] = data["days_since_last_evaluation"].fillna(
            -100
        )
        data["days_to_edu_end"] = data["days_to_edu_end"].fillna(-100)
        data["avg_daily_session_duration"] = data["avg_daily_session_duration"].fillna(
            0
        )

        # It's Learning Features
        data["number_of_sessions"] = data["number_of_sessions"].fillna(0)
        data["number_of_active_days"] = data["number_of_active_days"].fillna(0)
        data["avg_daily_session_duration"] = data["avg_daily_session_duration"].fillna(
            0
        )
        data["mode_earliest_hour_session"] = data["mode_earliest_hour_session"].fillna(
            0
        )
        data["median_earliest_hour_session"] = data[
            "median_earliest_hour_session"
        ].fillna(0)
        data["mode_latest_hour_session"] = data["mode_latest_hour_session"].fillna(0)
        data["median_latest_hour_session"] = data["median_latest_hour_session"].fillna(
            0
        )
        data["mode_number_of_course_sessions_per_day"] = data[
            "mode_number_of_course_sessions_per_day"
        ].fillna(0)
        data["median_number_of_course_sessions_per_day"] = data[
            "median_number_of_course_sessions_per_day"
        ].fillna(0)
        data["median_above_session_duration"] = data[
            "median_above_session_duration"
        ].fillna(0)
        data["min_above_session_duration"] = data["min_above_session_duration"].fillna(
            0
        )
        data["max_above_session_duration"] = data["max_above_session_duration"].fillna(
            0
        )

        return data

    def create_student_date_grid(self, stride: int = 1) -> pd.DataFrame:

        # Find first and last entry entry date for each student in data
        data = self.preprocessed_data.reset_index()
        index_table = (
            data.groupby("esas_studieforloebId")["Date"]
            .agg(start_date="min", end_date="max")
            .reset_index()
        )

        # Create date range from first date to last date with given <stride>
        index_table["date"] = index_table.apply(
            lambda x: pd.date_range(x["start_date"], x["end_date"], freq=f"{stride}D"),
            axis=1,
        )

        # Format index table by exploding pandas date_range and removing unused columns
        index_table.drop(columns=["start_date", "end_date"], inplace=True)
        index_table = index_table.explode("date")
        index_table.sort_values(by=["date", "esas_studieforloebId"], inplace=True)

        return index_table.set_index(["date", "esas_studieforloebId"])

    def compute_features(self) -> pd.DataFrame:

        # Get date grid containing all the date for each student to compute features
        student_date_grid = self.create_student_date_grid(stride=7)
        window_feature_engine = WindowFeatureEngine(
            self.preprocessed_data, student_date_grid
        )

        # Compute features for multiple windows with different lookback length
        window_features = window_feature_engine.compute_features_from_date_grid(30)

        # TODO: add other types of features and do a pd.merge

        # Fix missing values in features
        window_features = self._postprocess_data(window_features)

        return window_features
