import json
import os

import numpy as np

from dropout_prediction.feature_engineering.utils.config import (
    DATA_DIR,
    NAME_EDU_DISTANCE_FILE,
)

grade_mapping = {
    "12": "12",
    "10": "10",
    "4": "4",
    "7": "7",
    "02": "2",
    "00": "0",
    "-3": "-3",
    "G Godkendt": "Passed",
    "IG Ikke godkendt": "Failed",
    "IA Ikke afleveret": "Not submitted",
    "II Ikke Indstillet": "Failed",
    "SY Syg": "Sick",
    "IM Ikke mødt": "Failed",
    "LI Lovligt ikke indstillet": "Failed",
    "B Bestået": "Passed",
    "Bestået": "Passed",
    "IB Ikke bestået": "Failed",
    np.nan: np.nan,
}

grade_passed_mapping = {
    "12": "Passed",
    "10": "Passed",
    "4": "Passed",
    "7": "Passed",
    "02": "Passed",
    "00": "Failed",
    "-3": "Failed",
    "G Godkendt": "Passed",
    "IG Ikke godkendt": "Failed",
    "IA Ikke afleveret": "Failed",
    "II Ikke Indstillet": "Failed",
    "SY Syg": "Failed",
    "IM Ikke mødt": "Failed",
    "LI Lovligt ikke indstillet": "Failed",
    "B Bestået": "Passed",
    "Bestået": "Passed",
    "IB Ikke bestået": "Failed",
    np.nan: "Failed",
}


with open(os.path.join(DATA_DIR, NAME_EDU_DISTANCE_FILE), "r") as f:
    distance_to_campus_mapping = json.load(f)
