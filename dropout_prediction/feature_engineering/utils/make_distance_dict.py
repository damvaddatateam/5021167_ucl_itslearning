import json
import os
from typing import Dict

import geopandas as gpd
import haversine as hs

from dropout_prediction.feature_engineering.utils.config import (
    DATA_DIR,
    NAME_EDU_DISTANCE_FILE,
    NAME_EDUCATION_FILE,
    NAME_POSTAL_CODE_GEOJSON_FILE,
)

# Found from https://www.ucl.dk/uddannelser
city2education = {
    "Fredericia": ["Automationsteknolog", "Lærer"],
    "Jelling": ["Lærer", "Pædagog", "Lærer", "Pædagog"],
    "Svendborg": ["Pædagog", "Serviceøkonom", "Sygeplejerske"],
    "Vejle": [
        "Datamatiker",
        "Elinstallatør",
        "Handelsøkonom",
        "Logistikøkonom",
        "Procesteknolog",
        "Socialrådgiver",
        "Serviceøkonom",
        "Sofwareudvikling",
        "Sygeplejerske",
    ],
    "Odense": [
        "Automationsteknolog",
        "Lærer",
        "Pædagog",
        "Serviceøkonom",
        "Sygeplejerske",
        "Datamatiker",
        "Softwareudvikling",
    ],
}

# found from https://www.ucl.dk/optagelse-faq and google maps
city2zip = {
    "Odense": "5230",
    "Svendborg": "5700",
    "Jelling": "7300",
    "Vejle": "7100",
    "Fredericia": "7000",
}


def get_education2zip() -> Dict:
    """Create dictionary which links education and zip code of campus."""

    # read possible educations
    education_file = os.path.join(DATA_DIR, NAME_EDUCATION_FILE)
    with open(education_file) as f:
        educations = json.load(f)

    # find partial matches with education names found from website
    education2zip = {}
    for education in educations.values():
        cities = []
        for city, eds in city2education.items():
            matches = [(ed, city) for ed in eds if ed.lower() in education.lower()]
            # add all found matches
            if matches:
                cities.append(city2zip[city])
        # if there are no matches, assume that the education belongs in Odense
        if not cities:
            cities.append(city2zip["Odense"])

        education2zip[education] = cities

    return education2zip


def get_zip_distances() -> Dict:
    """Create dictionary which links all zip codes and the zip codes of the campuses."""

    postal_code_filename = os.path.join(DATA_DIR, NAME_POSTAL_CODE_GEOJSON_FILE)
    geo_df = gpd.read_file(postal_code_filename)
    geo_df = geo_df.set_index("POSTNR_TXT")

    # find centroids
    geo_df["x"] = geo_df["geometry"].centroid.x
    geo_df["y"] = geo_df["geometry"].centroid.y

    zip_distances = {}

    for zip1, row1 in geo_df.iterrows():
        coord1 = (row1["x"], row1["y"])

        for _, campus_zip in city2zip.items():

            # get mean of centroids corresponding to zip code
            coord_campus = (
                geo_df.loc[campus_zip]["x"].mean(),
                geo_df.loc[campus_zip]["y"].mean(),
            )

            dist = hs.haversine(coord1, coord_campus)
            zip_distances[(zip1, campus_zip)] = dist

    return zip_distances


def get_edzip2dist(education2zip: Dict, zip_distances: Dict) -> Dict:
    """Create nested dictionary which links education name (key, first level)
    and zipcode (key, seconds level) of student to distance."""

    edzip2dist = {education: {} for education in education2zip.keys()}

    for education, possible_campus_zips in education2zip.items():
        for (student_zip, _), _ in zip_distances.items():
            distances = [
                zip_distances[student_zip, potential_campus_zip]
                for potential_campus_zip in possible_campus_zips
            ]
            edzip2dist[education][student_zip] = min(distances)

    return edzip2dist


def make_distance_mapping():
    education2zip = get_education2zip()
    zip_distances = get_zip_distances()
    edzip2campus = get_edzip2dist(education2zip, zip_distances)

    edu_distance_filename = os.path.join(DATA_DIR, NAME_EDU_DISTANCE_FILE)
    with open(edu_distance_filename) as f:
        json.dump(edzip2campus, f)
