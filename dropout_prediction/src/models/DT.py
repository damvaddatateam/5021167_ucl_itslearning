import optuna
import sklearn.tree
from sklearn.metrics import f1_score,precision_score, recall_score, accuracy_score, matthews_corrcoef
from sklearn.model_selection import GroupKFold
import sklearn.ensemble as ensemble 

from dropout_prediction.src.features import preprocess

import joblib
import os
from statistics import mean
from tqdm import tqdm



def build_model(**kwargs):
    """ Insert your model here. """
    model = sklearn.tree.DecisionTreeClassifier(**kwargs)
    
    return model


def objective(trial):

    max_depth = trial.suggest_int('max_depth', 2, 32, log=True)
    min_samples_split  = trial.suggest_int('min_samples_split', 2, 4, log=True)
    class_weight = trial.suggest_categorical("class_weight", ["balanced"])

    model_params = {
        "max_depth" : max_depth, 
        "min_samples_split" : min_samples_split, 
        "class_weight": class_weight,
    }

    # # preprocess parameter
    strategy = trial.suggest_categorical(
         "strategy", ["esas_only"])

    # boolean_scale = trial.suggest_categorical("boolean_scale", [False])

    preprocess_args =         {
            "state": "train",
            "strategy": strategy, 
            "memory": 0
        }

    preprocessed_dict = preprocess.preprocess(**preprocess_args)
    print(preprocessed_dict.keys())
    features = preprocessed_dict["features"]
    labels = preprocessed_dict["labels"]
    groups = preprocessed_dict["groups"]
    print(features.shape, labels.shape, groups.shape)
    cv = GroupKFold(n_splits=5)

    print("starting cv loop")

    scores = []
    for train_idxs, val_idxs in tqdm(cv.split(features, labels, groups)):

        X_train = features.iloc[train_idxs]
        y_train = labels.iloc[train_idxs]
#        display(X_train)
        X_val = features.iloc[val_idxs]
        y_val = labels.iloc[val_idxs]

        clf = build_model(**model_params)

        clf.fit(X_train, y_train)
        y_pred = clf.predict(X_val)
        
        #score_n = calculate_metrics(data, val_idxs, y_pred)
        score_n = {
            "F1": f1_score(y_val, y_pred), 
            "Precision" : precision_score(y_val, y_pred), 
            "Recall" : recall_score(y_val, y_pred), 
            "MCC": matthews_corrcoef(y_val, y_pred)
        }
        print(score_n)
        scores.append(score_n["F1"])

    return mean(scores)


if __name__ == "__main__":

    study_name = os.path.basename(__file__).split(".")[0]
    print(study_name)

    # create and optimize study
    study = optuna.create_study(study_name=study_name, direction="maximize")
    study.optimize(objective, n_trials=100)
    joblib.dump(study, f"{study_name}.pkl")

    # print(study.best_trial)

    # train model on complete training set
    print(study.best_params)

    preprocessed_dict = preprocess.preprocess(**{
            "state": "train",
            "strategy": study.best_params["strategy"]
            }
    )

#    data = preprocessed_dict["data"]
    features = preprocessed_dict["features"]
    labels = preprocessed_dict["labels"]

    model_parameters = {
        key: value
        for key, value in study.best_params.items()
        if key not in ["state","strategy" ]
    }
    clf = build_model(**model_parameters)
    clf.fit(features, labels)
    joblib.dump(clf, f"{study_name}_full_training.pkl")

    clf = joblib.load(f"{study_name}_full_training.pkl")
    # # evaluate on test set:
    # preprocessed_dict = preprocess.preprocess(
    #     {
    #         "state": "test",
    #         "only_esas": study.best_params["only_esas"]
    #     }
    # )

    # data = preprocessed_dict["data"]
    # features = preprocessed_dict["features"]
    # labels = preprocessed_dict["labels"]
    # y_pred = clf.predict(features)
    # score_n = calculate_metrics(data, list(range(data.shape[0])), y_pred)["f1"]
    # print(f"Performance of best model on test set: \n{score_n}")

    # clf2 = pickle.loads(s)

