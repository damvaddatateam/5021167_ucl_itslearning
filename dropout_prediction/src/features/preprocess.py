import pandas as pd 
from typing import List
from sklearn.compose import make_column_transformer


KARAKTER_MAPPING = {
                "-3": -3.0, 
                "00": 0.0,
                "02": 2.0, 
                "04": 4.0, 
                "07": 7.0, 
                "10": 10.0, 
                "12": 12.0, 
                'B Bestået': None, 
                 'G Godkendt' : None, 
                 'IA Ikke afleveret': None, 
                 'IB Ikke bestået' : None, 
                 'IG Ikke godkendt': None, 
                 'II Ikke Indstillet' : None
                 }

def esas_karakter_mean(s = pd.Series)->float:
    """ Get mean of grades and ignore nan. If there are no grades, return -4. """
    return s.map(KARAKTER_MAPPING).mean().fillna(-4.0)



def weekly_agg_dict():
    d = {
        'esas_studiestart': "first", 
        'esas_forventet_afslutning': "first", 
        'esas_afgangsdato': "first", 
        'Uddannelse' : "first", 
        'esas_dst_kode': "first", 
#        'esas_rolle' : "first", 
        'esas_iso2' : "first",
#        'esas_studieemail' : "first",
        'esas_by' : "first" ,
        'esas_postnummer' : "first", 
        'esas_uddannelsestype' : "first", 
        'esas_cpr_personstatus' : "first", 
        'Uddannelses_label' : "first",
        'Person_label' : "first", 
        'Afgangsaarsag' : "first", 
        'esas_central_afgangsaarsag' : "first",
        'Praktik_Aflyst' : "any", 
#        'esas_bedoemmelsesdato' : "any" , we don't need this date
        'esas_bestaaet' : "sum"  ,
        'esas_taeller_som_forsoeg' : "sum",
        'esas_karakter' : esas_karakter_mean,  # TODO
        'inaktivperiode' : lambda x: pd.Series.mean(x, skipna=False), 
        'SessionDurationSeconds_min': "mean", 
        'SessionDurationSeconds_mean' : "mean",
        'SessionDurationSeconds_max': "mean", 
        'SessionHour_nunique': "mean", 
        'SessionHour_min'  : "mean",
        'SessionHour_max' : "mean", 
        'SessionStartTime_nunique' : "mean",
        'CourseSessionDurationMinutes_min' : "mean", 
        'CourseSessionDurationMinutes_mean': "mean",
        'CourseSessionDurationMinutes_max' : "mean", 
        'CourseSessionHour_nunique' : "mean",
        'CourseSessionHour_min' : "mean", 
        'CourseSessionHour_max' : "mean",
        'CourseSessionStartDate_nunique' : "mean", 
        'CourseId_nunique' : "mean",
        'AboveMeanSessionDuration_mean' : "mean", 
        'ElementSessionDurationMinutes_min' : "mean",
        'ElementSessionDurationMinutes_mean' :"mean",
        'ElementSessionDurationMinutes_max' : "mean", 
        'ElementSessionHour_nunique' : "mean",
        'ElementSessionHour_min' : "mean", 
        'ElementSessionHour_max' : "mean",
        'ElementSessionStartDate_nunique' : "mean", 
        'ElementCourseId_nunique' : "mean",
        'IsResource_mean' : "mean", 
        'IsActivity_mean' : "mean", 
        'IsAssessment_mean' : "mean",
        'AboveMeanElementSessionDuration_mean' : "mean", 
        'dropout_label' : "first", 
    }
    return d

def add_week_and_year(df: pd.DataFrame)->pd.DataFrame:
    """ Add columns containing week number and year to ease aggregation """
    # 
    df['Week'] = df['Date'].dt.isocalendar().week 
    df['Study_year'] = df['Date'].dt.isocalendar().year - df['esas_studiestart'].dt.isocalendar().year  
    return df


def esas_only(df_agg:pd.DataFrame):
    df_trimmed = df_agg.reset_index().groupby(['esas_studieemail']).agg('first')
    keeps = [ ] # remove 'Week'
    drops = [c for c in df_trimmed.columns if "esas" not in c and c not in keeps]
    drops.extend(["esas_bedoemmelsesdato",
                "esas_bestaaet",
                "esas_taeller_som_forsoeg",
                "esas_karakter", 
                'esas_afgangsdato' ])
    return keeps, drops,  df_trimmed

def preprocess(state = "train", strategy = "esas_only", memory =0):

    # extract arguments

    # read data  
    print(state) 
    df = pd.read_pickle(f"./dropout_prediction/data/clean/{state}_data.pkl")
 
    # list to hold features to be dropped
    drops = []

    df = add_week_and_year(df)

    df_agg = df.groupby(['esas_studieforloebId',"Study_year", 'Week']).agg('first')

    #print(df_agg.columns)

    keeps = ['Week']

    if strategy == "esas_only":        
        keeps, drops, df_trimmed = esas_only(df_agg)

    if strategy == "weekly" and memory == 0 :
        agg_dict = weekly_agg_dict()
        df_trimmed = df.groupby(['esas_studieemail','Week']).agg(agg_dict)
#        df_trimmed = df_agg.reset_index()
#        display(df_trimmed)        

#
    df_trimmed = df_trimmed.reset_index() 

    df_trimmed['esas_studiestart_week'] = df_trimmed['esas_studiestart'].dt.isocalendar().week  
    df_trimmed['esas_studiestart_week'] = pd.to_numeric(df_trimmed['esas_studiestart_week'])

    # convert datetimes to number
    date_cols = [ 
                'esas_forventet_afslutning'
                ]
    
#    display(df_trimmed[df_trimmed['esas_forventet_afslutning'].isna()])
    for c in date_cols: 
        df_trimmed[f"{c}_week"] = df_trimmed[c].dt.isocalendar().week 
        df_trimmed[f"{c}_study_year"] = df_trimmed[c].dt.isocalendar().year - df_trimmed["esas_studiestart"].dt.isocalendar().year
        df_trimmed = df_trimmed.drop(columns = [c])
#        df_trimmed[c] = pd.to_numeric(df_trimmed[c])

    df_trimmed = df_trimmed.drop(["esas_studiestart"], axis = "columns")

    df_trimmed['Week'] = pd.to_numeric(df_trimmed['Week'])

    drops.extend([ 'esas_studieforloebId',
                    'esas_studieemail', 
                    'Afgangsaarsag', 
                    'esas_central_afgangsaarsag', 
                    "esas_afgangsdato"
                            ])



    labels = df_trimmed['dropout_label']

    # get groups such that the same person will not be in both training and test set

    groups = df_trimmed['esas_studieemail']

    # remove redundant columns
    features = df_trimmed.drop( 
        columns = drops, 
        errors = 'ignore')

    # remove nan
    rows_with_nan = features.shape[0]
    na_rows = features.isna().any(axis = 1)
    features = features[~na_rows]
    labels = labels[~na_rows]
    groups = groups[~na_rows]
    rows_without_nan = features.shape[0]
    diff = rows_with_nan-rows_without_nan

    if diff: 
        print(f"WARNING: NaNs removed! This may lead to holes in the timeseries.")
        print(f"{diff} rows removed. This corresponds to {diff/rows_with_nan*100}%") 
        print(labels.shape, groups.shape, features.shape)

    # encode 
    features = pd.get_dummies(features)


    return {"features" : features, 

            "labels" : labels, 
            "groups" : groups}


if __name__ == "__main__":
    kwargs = {
            "state": "train",
            "strategy": "weekly", 
            "memory" : 0
        }
    preprocessed_dict = preprocess(**kwargs)
    # display(preprocessed_dict['features'])
    # display(preprocessed_dict['labels'])
    # display(preprocessed_dict['groups'])

