import glob
import pandas as pd
from pandas.core.base import DataError
from pandas.core.frame import DataFrame

def to_datetime(df:pd.DataFrame)->pd.DataFrame:
    """ Convert object columns to datetime. """
    print("Converting select columns to datetime")
    date_cols = [    'Date', 
                    'esas_studiestart',
                    'esas_forventet_afslutning',
                    'esas_afgangsdato', 
                    'esas_bedoemmelsesdato'
                    ]
    for c in date_cols:
        df[c] = pd.to_datetime(df[c])

    return df

def read_csv(path: str )->pd.DataFrame:

    all_files = glob.glob(path + "/*.csv")
    print(len(all_files))

    li = []

    for filename in all_files:
        #print(filename)
        df = pd.read_csv(filename, low_memory=False, header=0)
        #print(len(df))
        li.append(df)
    
    dataframe = pd.concat(li, axis=0)
    return dataframe




def fix_x_y(df: pd.DataFrame)->pd.DataFrame: 
    """Remove _y columns and move data in _x columns to the corresponding column without x """

    # columns containing ".._y"
    y_columns = list([c for c in df.columns if "_y" in c])
    # columns containing both ".._y" and ".._x"
    x_y_columns = x_y_columns = list([c for c in df.columns if "_x" in c or "_y" in c])
    columns_no_y = [ c[:-2] for c in y_columns]

    for y_c, no_y_c in zip(y_columns, columns_no_y):
       df[no_y_c] = df.loc[:, [no_y_c, y_c]].sum(axis=1, min_count = 1)
       #.fillna(0) + df[y_c].fillna(0)
 
    df = df.drop(x_y_columns, axis = 'columns')
    return df


def trim_data(df:pd.DataFrame) -> pd.DataFrame:
    # remove data from before july 21
    print("Removing data from people who started before '2021-07-21")
    df = df[df['esas_studiestart']>pd.to_datetime("2021-07-21")]
    df = df[df['Date']>pd.to_datetime("2021-07-21")]
    # remove UserId and ExternalUserId since it does not contain special information 
    # The two extra columns correspond to the values of 'UserId' and 'ExternalUserId', which refers to personal information of the students
    # and most of its values are zero. Not too relevant to predict if a student is going to drop out form a course?
    print("Removing 'UserId' and 'ExternalUserId', 'esas_iso2'")
    df = df.drop(['UserId', 'ExternalUserId', 'esas_iso2'], axis = 'columns')

    # collapse "_x" and "_y" columns
    print("Collapsing _x and _y columns")
    df = fix_x_y(df)

    return df

def fix_dtype(df:pd.DataFrame)->pd.DataFrame:
    df['esas_studieemail'] = df['esas_studieemail'].astype(str)
    df['esas_by'] = df['esas_by'].astype(str)
    df['esas_studieforloebId'] = df['esas_studieforloebId'].astype(str)
    df['Praktik_Aflyst'] = df['Praktik_Aflyst'].astype(bool)

    return df

def drop_redundant_features(df: pd.DataFrame)->pd.DataFrame:
    if df['Uddannelses_label'].notna().all():
        print("Dropping 'Uddannelses_label' as feature")
        df.drop(['Uddannelses_label'], axis = 'columns')
        
    return df