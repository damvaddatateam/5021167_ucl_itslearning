import dropout_prediction.src.data.import_data as import_data
from sklearn.model_selection import train_test_split
import pandas as pd
import pickle

def make_data(data_path):
    raw_data = import_data.read_csv(path)

    raw_data_dt = import_data.to_datetime(raw_data)
    
    raw_data_trimmed = import_data.trim_data(raw_data_dt)

    raw_data_fixed_dtype = import_data.fix_dtype(raw_data_trimmed)

    raw_data_drops = import_data.drop_redundant_features(raw_data_fixed_dtype)

    # get unique ids
    keys = raw_data_drops['esas_studieforloebId'].unique()

    # get dropout label - we need it for stratification
    labels = raw_data_drops.groupby('esas_studieforloebId')["Afgangsaarsag"].agg(lambda x: 1 if pd.DataFrame.notna(x).sum() else 0)#.apply(lambda x: 1 if len(x) > 1 else 0, axis = "columns")
    raw_data_drops['dropout_label'] = raw_data_drops['esas_studieforloebId'].map(labels)

    # stratified split into training and test set
    
    keys_train, keys_test, labels_train, labels_test = train_test_split(keys, labels, stratify = labels, test_size=0.2, random_state=76)

    train_data = raw_data_drops[raw_data_drops['esas_studieforloebId'].isin(keys_train)]
    test_data = raw_data_drops[raw_data_drops['esas_studieforloebId'].isin(keys_test)]

    train_data.to_pickle('./dropout_prediction/data/clean/train_data.pkl')        
    test_data.to_pickle('./dropout_prediction/data/clean/test_data.pkl')        


if __name__ == "__main__":
    path = 'dropout_prediction/data/raw/' 
    make_data(data_path=path)
    
 