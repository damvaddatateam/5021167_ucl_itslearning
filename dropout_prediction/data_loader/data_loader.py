import os
from datetime import datetime
from io import StringIO
from typing import List, Optional

import pandas as pd
from azure.storage.blob import BlobServiceClient
from tqdm import tqdm

from dropout_prediction.data_loader.config import (
    CONNECTION_STRING,
    CONTAINER_NAME,
    DATA_CACHE_DIR,
)
from dropout_prediction.data_loader.utils import get_date_from_blob_name


class UCLDataLoader:
    """Load data from Azure BLOB storage for dropout prediction"""

    def __init__(self, container_name: str = CONTAINER_NAME) -> None:
        self.container_name = container_name
        self.blob_service_client = self._initialize_blob_service_client()
        self.container_client = self.blob_service_client.get_container_client(
            container_name
        )

    def _initialize_blob_service_client(self) -> BlobServiceClient:
        """Helper funciton to initalize a blob service client"""
        conn_string = CONNECTION_STRING
        blob_service_client = BlobServiceClient.from_connection_string(conn_string)

        return blob_service_client

    def _get_all_blob_names_in_container(self) -> None:
        """Get the names of the existing the blobs in the container"""
        blobs = self.container_client.list_blobs()
        blob_names = [blob.name for blob in blobs]

        return blob_names

    def load_single_blob(self, blob_name: str) -> pd.DataFrame:
        """Load a single BLOB"""

        blob_client = self.container_client.get_blob_client(blob=blob_name)
        stream_downloader = blob_client.download_blob()
        stream = StringIO(stream_downloader.content_as_text())
        df = pd.read_csv(stream, low_memory=False, header=0)

        return df

    def load_data(
        self,
        start_date: Optional[str] = None,
        end_date: Optional[str] = None,
        use_cache: bool = True,
    ) -> pd.DataFrame:
        """Load all blobs and combine to a single dataframe"""

        # Check if data is available in cache
        if use_cache:
            dataset_name = f"ucl_data_{start_date}_to_{end_date}.csv"
            dataset_path = os.path.join(DATA_CACHE_DIR, dataset_name)
            if os.path.isfile(dataset_path):
                return pd.read_csv(dataset_path)

        # If cache is disabled or data is not in cache then load from Azure
        blobs_to_download = self._get_blobs_in_date_range(start_date, end_date)

        dataframe_list = []
        for blob_name in tqdm(blobs_to_download, desc="Downloading data"):
            single_blob_df = self.load_single_blob(blob_name)
            dataframe_list.append(single_blob_df)

        combined_data = pd.concat(dataframe_list, axis=0)

        # If cache is enabled save the date to file
        if use_cache:
            os.makedirs(DATA_CACHE_DIR, exist_ok=True)
            combined_data.to_csv(dataset_path, index=False)

        return combined_data

    def _get_blobs_in_date_range(
        self, start_date: Optional[str] = None, end_date: Optional[str] = None
    ) -> List[str]:
        """Get all the blob names from a given data range.
        If no start or end date is given all blobs are returned"""

        # Get all blob names
        all_blob_names = self._get_all_blob_names_in_container()
        all_blob_dates = [
            get_date_from_blob_name(blob_name) for blob_name in all_blob_names
        ]

        filtered_blob_names = list(zip(all_blob_names, all_blob_dates))
        # Filter out blobs before start_date
        if start_date:
            start_date = datetime.fromisoformat(start_date)
            filtered_blob_names = [
                (blob_name, date)
                for blob_name, date in filtered_blob_names
                if date >= start_date
            ]

        # Filter out blobs after end_date
        if end_date:
            end_date = datetime.fromisoformat(end_date)
            filtered_blob_names = [
                (blob_name, date)
                for blob_name, date in filtered_blob_names
                if date <= end_date
            ]

        filtered_blob_names = [blob_name for blob_name, date in filtered_blob_names]
        return filtered_blob_names
