import datetime
import re

BLOB_DATE_REGEX = re.compile(r"\d{4}-\d{2}-\d{2}")


def get_date_from_blob_name(blob_name: str) -> datetime.date:
    """Get a datetime.date from the blob name assuming the name format:
    blob_name = 'data_YEAR-MONTH-DAY.csv'
    """

    date_string = BLOB_DATE_REGEX.findall(blob_name)
    return datetime.datetime.fromisoformat(date_string[0])
