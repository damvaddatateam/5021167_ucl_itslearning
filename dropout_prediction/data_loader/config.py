import os

STORAGE_ACCOUNT_NAME = "frafaldsmodellering"
CONTAINER_NAME = "daglige-logs"
DATA_CACHE_DIR = "dropout_prediction/data_cache/"

ACCESS_KEY = os.getenv("UCL_ACCESS_KEY", None)
if ACCESS_KEY is None:
    raise ValueError("No value for UCL_ACCESS_KEY was found in environment variables")

CONNECTION_STRING = os.getenv("UCL_CONNECTION_STRING")

if CONNECTION_STRING is None:
    raise ValueError(
        "No value for CONNECTION_STRING was found in environment variables"
    )
