import os
from pprint import pprint
from statistics import mean
from typing import List, Optional

import joblib
import optuna
import pandas as pd
from sklearn.metrics import f1_score, matthews_corrcoef, precision_score, recall_score
from sklearn.model_selection import GroupKFold, train_test_split
from tqdm import tqdm

from dropout_prediction.models.config import (
    COLUMNS_NON_FEATURES,
    COLUMNS_TO_ONE_HOT_ENCODE,
    MODEL_DIR,
)


class ModelFactory(object):
    data: Optional[pd.DataFrame] = None
    groups: Optional[List[str]] = None
    labels: Optional[List[int]] = None

    def __init__(self, param_dict):
        self.param_dict = param_dict
        self.name = param_dict["name"]
        print("Instantiated ModelFactory with the following parameters:")
        pprint(param_dict)

    @staticmethod
    def _postprocess_data(data: pd.DataFrame) -> pd.DataFrame:
        """Fix missing values in features"""

        data["latest_grade"] = data["latest_grade"].fillna(-100)
        data["days_since_last_evaluation"] = data["days_since_last_evaluation"].fillna(
            -100
        )
        data["days_to_edu_end"] = data["days_to_edu_end"].fillna(-100)
        data["avg_daily_session_duration"] = data["avg_daily_session_duration"].fillna(
            0
        )

        return data

    def load_data(self, filename: str) -> None:
        data = pd.read_csv(filename)
        data["week"] = pd.to_numeric(pd.to_datetime(data["date"]).dt.isocalendar().week)
        data = self._postprocess_data(data)

        # Get studunts (groups) and dropout labels
        self.data = data
        self.groups = data["esas_studieforloebId"]
        self.labels = data["dropout_label"]

        features = data.drop(columns=COLUMNS_NON_FEATURES)
        features = pd.get_dummies(features, columns=COLUMNS_TO_ONE_HOT_ENCODE)
        self.features = features

    def _grouped_test_train_split(self, random_state: int = 76):

        student_labels = (
            self.data.groupby("esas_studieforloebId")["dropout_label"]
            .max()
            .reset_index()
        )
        students = student_labels["esas_studieforloebId"]
        labels = student_labels["dropout_label"]

        students_train, students_test, _, _ = train_test_split(
            students, labels, stratify=labels, test_size=0.2, random_state=random_state
        )

        # Get the row index of students in the train and test set
        train_ids = self.data[
            self.data["esas_studieforloebId"].isin(students_train)
        ].index
        test_ids = self.data[
            self.data["esas_studieforloebId"].isin(students_test)
        ].index

        return train_ids, test_ids

    def suggest_parameters(self, trial):
        suggested_param = {}  # param storage

        # int
        int_param = self.param_dict["model_param"].get("int", None)
        if int_param is not None:
            for k, v in int_param.items():
                suggested = trial.suggest_int(k, v["low"], v["high"], log=True)
                suggested_param.update({k: suggested})

        # categorical
        categorical_param = self.param_dict["model_param"].get("categorical", None)
        if categorical_param is not None:
            for k, v in categorical_param.items():
                suggested = trial.suggest_categorical(k, v)
                suggested_param.update({k: suggested})

        return suggested_param

    def optimization_function(self, trial):
        suggested_param = self.suggest_parameters(trial)

        # Split data into train and test set
        train_ids, _ = self._grouped_test_train_split()

        features = self.features.loc[train_ids]
        labels = self.labels.loc[train_ids]
        groups = self.groups[train_ids]

        cv = GroupKFold(n_splits=5)
        print("starting cv loop")

        scores = []
        for train_idxs, val_idxs in tqdm(cv.split(features, labels, groups)):

            X_train = features.iloc[train_idxs]
            y_train = labels.iloc[train_idxs]

            X_val = features.iloc[val_idxs]
            y_val = labels.iloc[val_idxs]

            clf = self.param_dict["model"](**suggested_param)

            clf.fit(X_train, y_train)
            y_pred = clf.predict(X_val)

            # score_n = calculate_metrics(data, val_idxs, y_pred)
            score_n = {
                "F1": f1_score(y_val, y_pred),
                "Precision": precision_score(y_val, y_pred),
                "Recall": recall_score(y_val, y_pred),
                "MCC": matthews_corrcoef(y_val, y_pred),
            }
            print(score_n)
            scores.append(score_n["F1"])

        return mean(scores)

    def optimize(self, direction, n_trials):

        # Find the best set op hyperparameters
        print("Optimizing hyperparameters")
        study = optuna.create_study(direction=direction)
        study.optimize(self.optimization_function, n_trials=n_trials)

        # Save study
        os.makedirs(MODEL_DIR, exist_ok=True)
        study_filename = os.path.join(MODEL_DIR, f"{self.name}.pkl")
        joblib.dump(study, study_filename)

        # Train model with best hyperparameters on full training set
        print("Training model on full training data set.")

        # Split data into train and test set
        train_ids, test_ids = self._grouped_test_train_split()

        features = self.features.loc[train_ids]
        labels = self.labels.loc[train_ids]

        # Set model parameters
        model_parameters = study.best_params

        clf = self.param_dict["model"](**model_parameters)
        clf.fit(features, labels)

        # Save model
        print("Saving model to file")
        joblib.dump(
            clf,
            os.path.join(MODEL_DIR, f"{self.name}_full_training.pkl"),
        )

        clf = joblib.load(os.path.join(MODEL_DIR, f"{self.name}_full_training.pkl"))

        return study.best_trial
