import sklearn.tree

from dropout_prediction.models.model_training import ModelFactory

my_dict = {
    "name": "DT",
    "model": sklearn.tree.DecisionTreeClassifier,
    "model_param": {
        "int": {
            "max_depth": {"low": 64, "high": 128},
            "min_samples_split": {"low": 2, "high": 4},
        },
        "categorical": {
            "class_weight": ["balanced"],
            "splitter": ["best", "random"],
            "criterion": ["gini", "entropy"],
        },
    },
}

n_trials = 100

my_optimizer = ModelFactory(my_dict)
my_optimizer.load_data("features.csv")
best_result = my_optimizer.optimize("maximize", n_trials)

print(f"best param: {best_result.params}")
print(f"best value: {best_result.values}")
