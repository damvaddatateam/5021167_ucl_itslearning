import sklearn.ensemble

from dropout_prediction.models.model_training import ModelFactory

my_dict = {
    "name": "RF",
    "model": sklearn.ensemble.RandomForestClassifier,
    "model_param": {
        "int": {"max_depth": {"low": 2, "high": 32}},
        "categorical": {"class_weight": ["balanced"], "n_estimators": [100]},
    },
}

n_trials = 20

my_optimizer = ModelFactory(my_dict)
my_optimizer.load_data("features.csv")
best_result = my_optimizer.optimize("maximize", n_trials)

print(f"best param: {best_result.params}")
print(f"best value: {best_result.values}")
