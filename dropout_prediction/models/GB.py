import sklearn.ensemble

from dropout_prediction.models.model_training import ModelFactory

my_dict = {
    "name": "GB2",
    "model": sklearn.ensemble.HistGradientBoostingClassifier,
    "model_param": {
        'int': {
            'max_depth': {
                'low': 2,
                'high': 32
            }},
        'categorical': {
        "learning_rate" : [0.1, 0.05, 0.02],
        "max_iter": [100]
    }
}}


n_trials = 20

my_optimizer = ModelFactory(my_dict)
my_optimizer.load_data("features.csv")
best_result = my_optimizer.optimize("maximize", n_trials)

# print(f"best param: {best_result.params}")
# print(f"best value: {best_result.values}")
