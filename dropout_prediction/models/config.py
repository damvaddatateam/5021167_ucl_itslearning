COLUMNS_TO_ONE_HOT_ENCODE = ["education", "latest_grade", "latest_grade_pass"]
COLUMNS_NON_FEATURES = ["date", "esas_studieforloebId", "dropout_date", "dropout_label"]
MODEL_DIR = "dropout_prediction/models/trained_models"
