import os
import pandas as pd

from dropout_prediction.preprocessing.config import (
    BOOLEAN_COLUMNS,
    DATETIME_COLUMNS,
    REDUNDANT_COLUMNS,
    STRING_COLUMNS,
)


def preprocess_pipeline(data: pd.DataFrame, save_to_file: bool = False) -> pd.DataFrame:
    """Run all pre-processing steps in the correct order"""

    processed_data = combine_columns(data)
    processed_data = correct_dtypes(processed_data)
    processed_data = drop_redundant_columns(processed_data)
    processed_data = replace_missing_values(processed_data)
    processed_data = create_dropout_variables(processed_data)

    if save_to_file:
        # Save to data cache, create cache if it does not exist
        DATA_CACHE_DIR = "dropout_prediction/data_cache/"
        os.makedirs(DATA_CACHE_DIR, exist_ok=True)

        filename = os.path.join(DATA_CACHE_DIR, "preprocessed_data.csv")
        processed_data.to_csv(filename)

    return processed_data


def combine_columns(data: pd.DataFrame) -> pd.DataFrame:
    """Data contains columns with similar names but with an _x or _y suffix.
    This function combines the columns using the sum aggregations"""

    # Find all columns with suffixes
    columns_with_suffix = [
        col for col in data.columns if col.endswith("_x") or col.endswith("_y")
    ]
    column_names_suffix_removed = list(set([col[:-2] for col in columns_with_suffix]))

    # Combine columns
    for col in column_names_suffix_removed:
        data[col] = data[[col, col + "_x", col + "_y"]].sum(axis=1, min_count=1)

    # Drop old columns with suffix
    data.drop(columns=columns_with_suffix, inplace=True)

    return data


def correct_dtypes(data: pd.DataFrame) -> pd.DataFrame:
    """Fix incorrect dtypes in dataframe coursed by pandas read csv"""

    data[DATETIME_COLUMNS] = data[DATETIME_COLUMNS].apply(
        pd.to_datetime, errors="coerce"
    )
    data[STRING_COLUMNS] = data[STRING_COLUMNS].astype(str)
    data[BOOLEAN_COLUMNS] = data[BOOLEAN_COLUMNS].astype(bool)
    return data


def drop_redundant_columns(data: pd.DataFrame) -> pd.DataFrame:
    """Drop redundant columns from the dataframe"""

    data.drop(columns=REDUNDANT_COLUMNS, inplace=True)
    return data


def replace_missing_values(data: pd.DataFrame) -> pd.DataFrame:
    """Handle missing values according to dtype"""

    # TODO: talk with Anders about what we need here
    return data


def create_dropout_variables(data: pd.DataFrame) -> pd.DataFrame:
    """Create a binary dropout variable and a dropout date variable"""

    # binary variable if student eventualle drops out
    data["dropout_label"] = data.groupby("esas_studieforloebId")[
        "Afgangsaarsag"
    ].transform(lambda x: 1 if x.notna().sum() else 0)

    # Actual drop out date
    data["dropout_date"] = data.groupby("esas_studieforloebId")[
        "esas_afgangsdato"
    ].transform("min")
    data.loc[data["dropout_label"] == 0, "dropout_date"] = pd.NaT

    return data
