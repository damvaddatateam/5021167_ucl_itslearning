DATETIME_COLUMNS = [
    "Date",
    "esas_studiestart",
    "esas_forventet_afslutning",
    "esas_afgangsdato",
    "esas_bedoemmelsesdato",
]

STRING_COLUMNS = ["esas_studieemail", "esas_by", "esas_studieforloebId"]

BOOLEAN_COLUMNS = ["Praktik_Aflyst"]

REDUNDANT_COLUMNS = [
    "Uddannelses_label",
]
