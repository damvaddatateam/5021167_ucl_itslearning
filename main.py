from dropout_prediction.data_loader.data_loader import UCLDataLoader
from dropout_prediction.preprocessing.preprocess import preprocess_pipeline
from dropout_prediction.feature_engineering.feature_factory import FeatureFactory


# Constants
START_DATE = "2021-08-01"
END_DATE = "2022-01-31"

# Data loading
print("Loading data")
data_loader = UCLDataLoader()
raw_data = data_loader.load_data(START_DATE, END_DATE)

# Data pre-processing
print("Preprocessing data")
processed_data = preprocess_pipeline(raw_data)

# Feature engineering
print("Computing features")
feature_factory = FeatureFactory(processed_data)
features = feature_factory.compute_features()
features.to_csv("features.csv")
print(features)
